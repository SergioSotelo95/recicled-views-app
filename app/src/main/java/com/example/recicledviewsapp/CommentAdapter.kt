package com.example.recicledviewsapp

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.recicledviewsapp.databinding.CommentsListBinding
import com.example.recicledviewsapp.databinding.PostListBinding

class CommentAdapter : RecyclerView.Adapter<CommentAdapter.CommentHolder>() {

    inner class CommentHolder(val binding: CommentsListBinding) :
        RecyclerView.ViewHolder(binding.root)

    private val diffCallback = object : DiffUtil.ItemCallback<CommentObject>() {
        override fun areItemsTheSame(oldItem: CommentObject, newItem: CommentObject): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: CommentObject, newItem: CommentObject): Boolean {
            return oldItem == newItem
        }
    }

    private val differ = AsyncListDiffer(this, diffCallback)

    var allComments: List<CommentObject>
        get() = differ.currentList
        set(value) {
            differ.submitList(value)
        }

    override fun getItemCount(): Int = allComments.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentHolder {
        return CommentHolder(
            CommentsListBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: CommentHolder, position: Int) {
        holder.binding.apply {
            val comment = allComments[position]
            TVName.text = comment.name
            TVComment.text = comment.body
        }
    }
}