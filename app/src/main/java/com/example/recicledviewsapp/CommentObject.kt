package com.example.recicledviewsapp

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class CommentObject(
    val body: String,
    val email: String,
    @PrimaryKey
    val id: Int,
    val name: String,
    val postId: Int
)