package com.example.recicledviewsapp

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

interface APIService {
    @GET("/posts")
    suspend fun getPosts(): Response<List<PostObject>>

    @GET("/comments")
    suspend fun getComments(@Query("postId") postId:Int): Response<List<CommentObject>>
}