package com.example.recicledviewsapp


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.work.PeriodicWorkRequest
import androidx.work.WorkManager
import com.example.recicledviewsapp.databinding.ActivityMainBinding
import retrofit2.HttpException
import java.io.IOException
import java.util.concurrent.TimeUnit

const val TAG = "MainActivity"

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private lateinit var postAdapter: PostAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupRecyclerView()
        refresher()
        setPeriodicWorkRequest()

        lifecycleScope.launchWhenCreated {
            binding.ProgressBar.isVisible = true
            val response = try {
                RetrofitInstance.api.getPosts()
            } catch (e: IOException) {
                Log.e(TAG, "IOException")
                binding.ProgressBar.isVisible = false
                return@launchWhenCreated
            } catch (e: HttpException) {
                Log.e(TAG, "HttpException, unexpected response")
                binding.ProgressBar.isVisible = false
                return@launchWhenCreated
            }
            if (response.isSuccessful && response.body() != null) {
                postAdapter.allPosts = response.body()!!
            } else {
                Log.e(TAG, "Response not successful")
            }
            binding.ProgressBar.isVisible = false
        }
    }

    private fun setupRecyclerView() = binding.RV.apply {
        postAdapter = PostAdapter(0)
        adapter = postAdapter
        layoutManager = LinearLayoutManager(this@MainActivity)
    }

    private fun refresher() {
        val swipe: SwipeRefreshLayout = findViewById(R.id.swipeRefreshLayout)
        swipe.setOnRefreshListener {
            //Add insert to database
            swipe.isRefreshing = false
        }
    }

    private fun setPeriodicWorkRequest() {
        val periodicWorkRequest = PeriodicWorkRequest
            .Builder(PostWorker::class.java, 15, TimeUnit.MINUTES)
            .build()

        WorkManager.getInstance(applicationContext).enqueue(periodicWorkRequest)
    }

}