package com.example.recicledviewsapp

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class PostObject(
    val body: String,
    @PrimaryKey
    val id: Int,
    val title: String,
    val userId: Int
)