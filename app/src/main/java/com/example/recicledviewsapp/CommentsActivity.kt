package com.example.recicledviewsapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.recicledviewsapp.databinding.ActivityCommentsBinding
import retrofit2.HttpException
import java.io.IOException

class CommentsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityCommentsBinding

    private lateinit var commentsAdapter: CommentAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCommentsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupRecyclerView()


        lifecycleScope.launchWhenCreated {
            binding.ProgressBar.isVisible = true
            val response = try {
                RetrofitInstance.api.getComments(intent.extras?.getInt("postId") ?: 1)
            } catch (e: IOException) {
                Log.e(TAG, "IOException")
                binding.ProgressBar.isVisible = false
                return@launchWhenCreated
            } catch (e: HttpException) {
                Log.e(TAG, "HttpException, unexpected response")
                binding.ProgressBar.isVisible = false
                return@launchWhenCreated
            }
            if (response.isSuccessful && response.body() != null) {
                commentsAdapter.allComments = response.body()!!
            } else {
                Log.e(TAG, "Response not successful")
            }
            binding.ProgressBar.isVisible = false
        }
    }

    private fun setupRecyclerView() = binding.CommentRV.apply {
        commentsAdapter = CommentAdapter()
        adapter = commentsAdapter
        layoutManager = LinearLayoutManager(this@CommentsActivity)
    }
}