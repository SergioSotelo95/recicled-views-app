package com.example.recicledviewsapp

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.recicledviewsapp.databinding.PostListBinding
import java.security.AccessController.getContext


class PostAdapter(var postId: Int) : RecyclerView.Adapter<PostAdapter.PostHolder>() {

    inner class PostHolder(val binding: PostListBinding) : RecyclerView.ViewHolder(binding.root)

    private val diffCallback = object : DiffUtil.ItemCallback<PostObject>() {
        override fun areItemsTheSame(oldItem: PostObject, newItem: PostObject): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: PostObject, newItem: PostObject): Boolean {
            return oldItem == newItem
        }
    }

    private val differ = AsyncListDiffer(this, diffCallback)

    var allPosts: List<PostObject>
        get() = differ.currentList
        set(value) {
            differ.submitList(value)
        }

    override fun getItemCount(): Int = allPosts.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostHolder {
        return PostHolder(
            PostListBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: PostHolder, position: Int) {
        holder.binding.apply {
            val post = allPosts[position]
            TVTitle.text = post.title
            TVBody.text = post.body
            postId = post.id
            val context = postLayout.context
            postLayout.setOnClickListener {
                val intentComments = Intent(context, CommentsActivity::class.java)
                intentComments.putExtra("postId", postId)
                startActivity(context, intentComments, null)
            }
        }
    }

}
